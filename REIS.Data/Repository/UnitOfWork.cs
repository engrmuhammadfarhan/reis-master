﻿using REIS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REIS.Data.Repository
{
    public class UnitOfWork : IDisposable
    {
        private REISEntities context = new REISEntities();

        //private GenericRepository<ErrorLog> logRepository;


        //private GenericRepository<SP_CallTagReport_Result> _SPCallTagRepository;

        //public GenericRepository<ErrorLog> ErrorLogRepository
        //{
        //    get
        //    {
        //        if (this.errorLogRepository == null)
        //        {
        //            this.errorLogRepository = new GenericRepository<ErrorLog>(context);
        //        }
        //        return errorLogRepository;
        //    }
        //}

        //public GenericRepository<SP_CallTagReport_Result> SP_CallTagReport
        //{
        //    get
        //    {
        //        if (this._SPCallTagRepository == null) { this._SPCallTagRepository = new GenericRepository<SP_CallTagReport_Result>(context); }
        //        return _SPCallTagRepository;
        //    }
        //}

        public void Save()
        {
            context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
