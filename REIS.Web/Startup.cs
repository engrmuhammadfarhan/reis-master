﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(REIS.Web.Startup))]
namespace REIS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
